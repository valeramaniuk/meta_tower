import boto3
import time

sqs = boto3.resource('sqs', region_name = "us-west-2")
queue = sqs.get_queue_by_name(QueueName='slack_outbound')

queue.send_message(MessageBody="command",
                   MessageAttributes={
                           'command': {
                                          "StringValue": "report",
                                          "DataType": "String"
                                            },
                           'search_term': {
                                          "StringValue": "uid=oc4e042a61",
                                          "DataType": "String"
                                            },
                           'service': {
                                          "StringValue": "ldap",
                                          "DataType": "String"
                                            }
                                        }
                    )
