import platform
from slack import post
from sqs import recieve_message
from acsiiart import acsiart
from command_tree import parse_command


def main():

    # Display Logo
    print(acsiart)

    # Send the confirmation to Slack on startup
    # reports the hostname (cross-platform)
    post("META+Tower is online now\n Host: {}".format(platform.uname()[1]))
    print("Confirmation has been sent to Slack. If you can't see is - there is a problem.\n")

    # the daemon start
    #   - gets the message
    #   - if it's a 'command' type of a message
    #       - spin the thread to deal wiith it
    #       - TODO: actually as of now everything is serialized, no threads
    while True:
        body, attr = recieve_message()
        if body == 'command':
            parse_command(attr)

if __name__ == '__main__':
    main()
