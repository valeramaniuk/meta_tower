from commands import ldapsearch, ping, portscan
from slack import post


def parse_command(attr):

    command = attr['command']['StringValue']

    if command == '/ldapsearch':
        ldapsearch.main(attr)

    elif command == '/ping':
        ping.main(attr)

    elif command == '/portscan':
        portscan.main(attr)
    else:
        post('Unknown command')
