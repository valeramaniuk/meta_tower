from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError
from dynamo import get_service_configuration
import json

# TODO: this is SECRET 
# HOOK_URL ="permanently disabled"
SLACK_CHANNEL = "#general"


def post(message):

    conf = get_service_configuration("slack")
    WEBHOOKS =  conf['webhooks']
    HOOK_URL = ""
    for webhook in WEBHOOKS:
        if webhook['webhook_name'] == "META+Tower":
            HOOK_URL = webhook['hook_url']

    if not HOOK_URL:
        return False

    slack_message = {
            'text': message
        }

    req = Request(HOOK_URL, json.dumps(slack_message).encode('utf-8'))

    try:
        response = urlopen(req)
        response.read()

    except:
        print("Posting to slack have just failed\n")

        #logger.info("Message posted to %s", slack_message['channel'])
    #except HTTPError as e:
        #logger.error("Request failed: %d %s", e.code, e.reason)
    #except URLError as e:
        #logger.error("Server connection failed: %s", e.reason)