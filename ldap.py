from ldap3 import Server, Connection,  ALL
from dynamo import get_service_configuration


def ldap_search(search_term):

    ldap_conf = get_service_configuration("ldap")
    HOST = ldap_conf['host']
    ROOTDN = ldap_conf['rootDN']
    ROOTPW = ldap_conf['rootPW']
    BASE = ldap_conf['base']

    server = Server(HOST, get_info=ALL)
    conn = Connection(server, ROOTDN, ROOTPW, auto_bind=True)
    if not conn:
        response = "I cannot even connect to it\n"
        return response

    is_found = conn.search(BASE, "({})".format(search_term), attributes=['sn', 'displayName', 'givenName', 'userpassword'])
    return is_found, conn.entries





