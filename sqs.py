import boto3

sqs = boto3.resource('sqs', region_name = "us-west-2")
queue = sqs.get_queue_by_name(QueueName='slack_outbound')

# queue.send_message(MessageBody = "LastEvaluatedKey",
#                    MessageAttributes ={
#                            'class_number':{
#                                           "StringValue":"Value value ",
#                                           "DataType":"String"
#                                             }
#                                         }
#                     )


def recieve_message():
    messages = queue.receive_messages(
                                      MaxNumberOfMessages=1,
                                      MessageAttributeNames=['All']
                                    )
    if len(messages) > 0:
        message = messages[0]
        body, attr = message.body, message.message_attributes
        message.delete()
        return body, attr
    else:
        return None, None