# ChatOps POC #
Available monitoring tools like Chef, Ansible, Prometheus, etc. couldn't be easily used because of the strict company's firewall our team had absolutely no control of. 
This is a proof-of-concept of a Slack bot based on AWS Lambda and DynamoDB to conveniently manage/monitor our on-prem infrastructure. A reverse proxy is implemented on AWS SQS.
Sample features:

1. Ping
2. Port Scan
3. LDAP utils 
3. AWS infra  report


![](https://s3-us-west-2.amazonaws.com/valera-public-bucket-78/META%2BTOWER.png)