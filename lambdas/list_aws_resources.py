'''
Lists all EC2 instances in the account anr returns to Slack
'''

import boto3
import json
import logging
import os

from base64 import b64decode
from urllib.parse import parse_qs


ENCRYPTED_EXPECTED_TOKEN = os.environ['kmsEncryptedToken']

kms = boto3.client('kms')
expected_token = kms.decrypt(CiphertextBlob=b64decode(ENCRYPTED_EXPECTED_TOKEN))['Plaintext'].decode('utf-8')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def respond(err, res):

    return {
        'statusCode': err,
        'body': json.dumps(res),
        'headers': {
            'Content-Type': 'application/json',
        },
    }


def get_all_ec2(tags=None):

    message = ""
    ec2 = boto3.resource('ec2', region_name="us-west-2")
    instances = ec2.instances.all()
    instance_name = ""

    for instance in instances:
        for tag in instance.tags:
            if tag["Key"] == "Name":
                instance_name = tag["Value"]
        message += "*{2}* {1} {0}\n".format(instance.id, instance.instance_type, instance_name)
    return message


def get_all_s3_buckets(tags=None):

    message = ""
    s3client = boto3.client('s3')
    buckets = s3client.list_buckets()['Buckets']
    bucket_list = []


    for bucket in buckets:
        bucket_list.append((bucket['Name'], bucket['CreationDate']))
    bucket_list.sort(key = lambda tup: tup[1])

    for bucket in bucket_list:
        message+="{} {}\n".format(bucket[0], bucket[1].strftime("%Y-%m-%d"))
    return message


def lambda_handler(event, context):
    params = parse_qs(event['body'])
    token = params['token'][0]
    if token != expected_token:
        logger.error("Request token (%s) does not match expected", token)
        return respond('400', 'Invalid request token')

    #user = params['user_name'][0]
    #command = params['command'][0]
    #channel = params['channel_name'][0]
    #if 'text' in params:
    #    command_text = params['text'][0]
    #else:
    #    command_test = ""

    message = ""
    message += "\n*EC2 instances:*\n"
    message +=get_all_ec2()

    message += "\n*S3 Buckets:*\n"
    message += get_all_s3_buckets()

    if not message:
        message = "Nothing to list"
    return respond ("200", message)

