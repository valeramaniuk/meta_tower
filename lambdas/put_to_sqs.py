'''
Lambda function whoch accepts a command from Slack
translates it to applicabel service format, specifies attributes
and posts as s message to SQS
'''


import boto3
import json
import logging
import os

from base64 import b64decode
from urllib.parse import parse_qs
# from lambdas.event_test import slack_event

ENCRYPTED_EXPECTED_TOKEN = os.environ['kmsEncryptedToken']

kms = boto3.client('kms')
expected_token = kms.decrypt(CiphertextBlob=b64decode(ENCRYPTED_EXPECTED_TOKEN))['Plaintext'].encode('utf-8')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

sqs = boto3.resource('sqs', region_name = "us-west-2")
queue = sqs.get_queue_by_name(QueueName='slack_outbound')

def form_sqs_message(params):

    user = params['user_name'][0]
    command = params['command'][0]
    channel = params['channel_name'][0]
    if 'text' in params:
        command_text = params['text'][0]
    else:
        command_text = "none"
    response_url = params['response_url'][0]

    message={
        'command': {
                  "StringValue": command,
                  "DataType": "String"
                  },
        'command_text': {
                  "StringValue": command_text,
                  "DataType": "String"
                  },
        'user': {
                  "StringValue": user,
                  "DataType": "String"
                  },
        'response_url': {
                  "StringValue": response_url,
                  "DataType": "String"
                  },
        'channel': {
                  "StringValue": channel,
                  "DataType": "String"
                  }
    }
    return message


def post_to_sqs(message):
    return queue.send_message(
        MessageBody="command",
        MessageAttributes=message
    )


def respond(err, res):
    return {
        'statusCode': err,
        'body': "Failed!" if err != '200' else json.dumps(res),
        'headers': {
            'Content-Type': 'application/json',
        },
    }


def lambda_handler(event, context):
    params = parse_qs(event['body'])
    token = params['token'][0]
    if token != expected_token:
        logger.error("Request token (%s) does not match expected", token)
        return respond('404','Invalid request token')

    sqs_message = form_sqs_message(params)
    err = post_to_sqs(sqs_message)['ResponseMetadata']['HTTPStatusCode']

    return respond(str(err), "got it!")

