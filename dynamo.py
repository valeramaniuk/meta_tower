import boto3

dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
configuration_table = dynamodb.Table('metachat_conf')

def get_service_configuration(service):

    response = configuration_table.get_item(
        Key={
            'service': service
        }
    )
    try:
        if response['Item']['values']:
            #print('Getting {} config SUCCESSFUL'.format(service))

            return response['Item']['values']
        else:
            #print('Getting {} config FAILED'.format(service))
            return False
    except KeyError:
        #print('Getting {} config FAILED'.format(service))
        return False
