from ldap3 import Server, Connection, ALL
from ldap3.core.exceptions import LDAPInvalidFilterError, LDAPBindError
from dynamo import get_service_configuration
from slack import post


def main(attr):
    search_term = attr['command_text']['StringValue']

    '''
    is the search term is "test"
    the search will be launched for entries KNOWN to be present in the particular
    backend
    '''
    if search_term == '##test':
        ldap_backend_test()
        return

    is_found, message = ldap_search(search_term)

    if is_found:
        # if found then the message would contain the entries
        post(message)
        return

    else:
        # if it was not found
        # then the message contains the reason
        post(message)
        return


def ldap_search(search_term):

    ldap_conf = get_service_configuration("ldap")
    HOST = ldap_conf['host']
    ROOTDN = ldap_conf['rootDN']
    ROOTPW = ldap_conf['rootPW']
    BASE = ldap_conf['base']

    server = Server(HOST, get_info=ALL)

    # If no one will assign a new value to it - the logic is broken
    message = "If you can see me - check ldap.main.ldap_search\n"

    try:
        conn = Connection(server, ROOTDN, ROOTPW, auto_bind=True)

    except LDAPBindError as err:
        is_found = False
        message = "Credentials are probably wrong, check conf table\n{}\n".format(err)
        return is_found, message

    try:
        is_found = conn.search(BASE, "({})".format(search_term), attributes=['sn', 'displayName', 'givenName', 'userpassword'])

        for entry in conn.entries:
            message = "{}\n".format(str(entry))

        return is_found, message

    except LDAPInvalidFilterError as err:

        is_found = False
        message = "The search term is terribly wrong fo some resaon. I don't know why yet\n{}\n".format(err)
        return is_found, message
    except Exception as err:
        return False, str(err)


def ldap_backend_test():

    ldap_conf = get_service_configuration("ldap")
    BACKENDS = ldap_conf['backends']

    response = ""

    for backend in BACKENDS:

        is_found, entries = ldap_search(backend['search_term'])
        if is_found:
            response += "Backend '{}' responded\n".format(backend['name'])
        else:
            response += "Backend '{}' *NOT responded*\n".format(backend['name'])
    post(response)
