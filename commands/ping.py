from platform import system as system_name # Returns the system/OS name
from os import system as system_call       # Execute a shell command
from slack import post


def main(attr):

    # TODO: check why it may by so
    # Very retarded error
    # on syntax check it assumes the attr is a string

    if type(attr).__name__ == 'dict':
        hosts = attr['command_text']['StringValue'].split(' ')
        if "##discover" in hosts:
            discover_by_ping()
            return

        for host in hosts:
            # because of the split one of the hosts may be an empty string
            if host:
                message = "{} ping ".format(host)
                result=ping(host)
                if result:
                    message += "*OK!*\n"
                else:
                    message += '*FAIL!*\n'
                post(message)
                return


def discover_by_ping():
    for ip in range(256):
        host = "130.166.38."+str(ip)
        responded=ping(host)
        if responded:
            post("{} pings".format(host))
    return

def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that some hosts may not respond to a ping request even if the host name is valid.
    https://stackoverflow.com/questions/2953462/pinging-servers-in-python
    """
    print(host)
    # Ping parameters as function of OS
    parameters = "-n 1" if system_name().lower() == "windows" else "-c 1"

    # Pinging
    return system_call("ping " + parameters + " " + host) == 0


main('')