import socket
import re

from slack import post


def is_ip(address):

    p = re.compile('^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}$')
    return p.search(address) is not None


def check_port(remoteserver_ip, port):

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    sock.settimeout(1)
    result = sock.connect_ex((remoteserver_ip, port))
    sock.close()
    return result == 0


def resolve_host(remoteserver):
    '''
      Cheking if the address is a DNS record or is an IP
    '''
    if is_ip(remoteserver):
        return remoteserver
    else:
        try:
            remoteserver_ip = socket.gethostbyname(remoteserver)
            return remoteserver_ip
        except:
            return None

def post_error_msg(text):
    if text == 'port':
        post("Ports specified in an unrecognizable format\n")

    return


def main(attr):

    # expects <host addr or IP> <port or port1,port2,.. or port1-port2>
    attr = attr['command_text']['StringValue'].split(' ')
    remoteserver = attr[0]

    remoteserver_ip = resolve_host(remoteserver)
    if not remoteserver_ip:
        post("Cannot resolve the host\n")
        return

    '''
    Checking if there is something after the host
    and if there is - parsing it
        expecting
        100,101
        100-105
        100
    '''
    if len(attr) > 1:

        if "," in attr[1]:
            ports = attr[1].split(',')

        elif "-" in attr[1]:
            port_range = attr[1].split('-')

            try:
                ports = range(int(port_range[0]), int(port_range[1])+1)
            except:
                post_error_msg('port')
                return

        elif attr[1].isdigit():

            try:
                ports = [int(attr[1])]
            except:
                post_error_msg('port')
                return

        else:
            post_error_msg('port')
            return
    else:
        post("Specify ports!")
        return

    '''
        If there are too many ports - hide negative responses
        '''
    if len(ports) > 3:
            limit_output = True
    else:
        limit_output = False

    for port in ports:
        '''
        Check ports one by one reporting on every
        '''
        try:
            port = int(port)
        except:
            post_error_msg('port')
            continue

        scan_result = check_port(remoteserver_ip, port)
        if scan_result:
            post("{} Port {}: 	 Open".format(remoteserver, port))
        else:
            if not limit_output:
                post("{} Port {}: 	 *Closed*".format(remoteserver, port))

    if limit_output:
        post("{} *end of scan*".format(remoteserver))
    return
